import java.io.IOException;
import java.net.InetSocketAddress;
import java.io.OutputStream;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpExchange;

public class HTTPServer {

    // Server based on instructions from the following post:
    // https://stackoverflow.com/questions/3732109/simple-http-server-in-java-using-only-java-se-api
    // I then made my version out of it.
    public static void main(String[] args) throws Exception {

        Integer portNumber = 8080;

        try {
            HttpServer httpServer = HttpServer.create(new InetSocketAddress(portNumber), 0);
            httpServer.createContext("/main", new ServerHandler());
            httpServer.setExecutor(null);
            httpServer.start();
        } catch (IOException exception) {
            System.err.println(exception.toString());
        }

        System.out.println("Server is running on port: " + portNumber);
    }

    static class ServerHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {

            String tolkien = "J. R. R. Tolkien";
            String bookName = "The Lord of The Rings - The Fellowship of the Ring";

            Book fellowship = buildBook(tolkien, bookName);

            String bookAsJson = "";

            try {
                bookAsJson = bookToJson(fellowship);
            } catch (JsonProcessingException exception) {
                System.err.println(exception.toString());
            }

            httpExchange.sendResponseHeaders(200, bookAsJson.getBytes().length);
            OutputStream outputStream = httpExchange.getResponseBody();
            outputStream.write(bookAsJson.getBytes());
            outputStream.close();
        }
    }

    public static String bookToJson(Book book) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(book);

        return json;
    }

    public static Book buildBook(String author, String bookName) {
        Book book = new Book();
        book.setName(bookName);
        book.setAuthor(author);

        return book;
    }

}