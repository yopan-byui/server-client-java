import java.net.*;
import java.io.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class HTTPClient {

    // Client based on the instructions by Brother Tuckett
    public static void main(String[] args) {
        String url = "http://localhost:8080/main";
        String response = HTTPClient.getHttpContent(url);
        Book book = jsonToBook(response);

        System.out.println(book);
    }

    public static String getHttpContent(String string) {
        String json = "{\"key\":\"value\", \"key\":\"value\"}";

        try {

            URL url = new URL(string);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoOutput(true);

            try(OutputStream outputStream = httpURLConnection.getOutputStream()) {
                byte[] input = json.getBytes("UTF-8");
                outputStream.write(input, 0, input.length);
            } catch (Exception exception) {
                System.err.println(exception.toString());
            }

            String data = "";

            try(BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(httpURLConnection.getInputStream(), "UTF-8"))
            ) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;

                while ((responseLine = bufferedReader.readLine()) != null) {
                    response.append(responseLine.trim());
                }

                data = response.toString();

            } catch (Exception exception) {
                System.err.println(exception.toString());
            }

            return data;

        } catch (IOException e) {
            System.err.println(e.toString());
        }

        return "Error";
    }

    public static Book jsonToBook(String json) {
        ObjectMapper objectMapper = new ObjectMapper();
        Book book = null;

        try {
            book = objectMapper.readValue(json, Book.class);
        } catch (JsonProcessingException exception) {
            System.err.println(exception.toString());
        }

        return book;
    }
}
